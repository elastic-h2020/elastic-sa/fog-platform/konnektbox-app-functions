package cnfpipe

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/edgexfoundry/app-functions-sdk-go/appcontext"
	"github.com/edgexfoundry/go-mod-core-contracts/models"
)

type responseReadings struct {
	Value string
}

type responseEvent struct {
	Readings []responseReadings
}

type dataClayJSON struct {
	Timestamp string `json:"timestamp"`
	Device    string `json:"device"`
	Value     string `json:"value"`
}

/*

	This pipeline function extract the "value" content from the first reading of
	the JSON.

*/
func getReadingValue(edgexcontext *appcontext.Context, params ...interface{}) (continuePipeline bool, stringType interface{}) {

	if len(params) < 1 {
		// We didn't receive a result
		return false, nil
	}

	event := responseEvent{}
	json.Unmarshal([]byte(params[0].(string)), &event)
	//fmt.Println(event.Readings[0].Value)
	edgexcontext.Complete([]byte(params[0].(string)))

	return true, string(event.Readings[0].Value)
}

/*

	This pipeline function extracts sends a dataClayJSON to the dataclay service if avaiable


*/
func postDataClay(edgexcontext *appcontext.Context, params ...interface{}) (continuePipeline bool, stringType interface{}) {

	if len(params) < 1 {
		// We didn't receive a result
		return false, nil
	}

	event := models.Event{}
	json.Unmarshal([]byte(params[0].(string)), &event)
	msg := dataClayJSON{}

	msg.Timestamp = time.Now().String()
	msg.Device = event.Readings[0].Device
	msg.Value = event.Readings[0].Value
	client := http.Client{Timeout: 5 * time.Second}
	postURL := "http://dataclay-client-service:8000/dataclay/reading"
	bmsg, err := json.Marshal(msg)
	resp, err := client.Post(postURL, "application/json", bytes.NewBuffer(bmsg))

	/* If daclay is down notify and continue the pipeline*/
	if err != nil {
		println("Dataclay service not Responding")
		return true, params[0].(string)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(body))

	return true, params[0].(string)

}
