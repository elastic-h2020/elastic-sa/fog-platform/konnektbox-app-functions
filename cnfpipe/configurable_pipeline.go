package cnfpipe

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/edgexfoundry/app-functions-sdk-go/appcontext"
	"github.com/edgexfoundry/app-functions-sdk-go/appsdk"
	"github.com/edgexfoundry/app-functions-sdk-go/pkg/transforms"
	"github.com/edgexfoundry/go-mod-core-contracts/clients/logger"
	"github.com/edgexfoundry/go-mod-core-contracts/models"
)

/*

ConfigurablePipeline stores the functions pipeline and a map with the positions
of each function

*/
type ConfigurablePipeline struct {
	functions []appcontext.AppFunction
	positions map[string]int
	sdk       *appsdk.AppFunctionsSDK
}

/* Pipeline handle functions */

func (pipe *ConfigurablePipeline) addFunction(functionName string, nFuntionPtr appcontext.AppFunction) error {

	_, exists := pipe.positions[functionName]
	if exists {
		return errors.New("Function already exists")
	}

	position := len(pipe.functions)
	pipe.positions[functionName] = position
	pipe.functions = append(pipe.functions, nFuntionPtr)
	return nil

}

func (pipe *ConfigurablePipeline) updateFunction(functionName string, nFuntionPtr appcontext.AppFunction) error {
	position, exists := pipe.positions[functionName]
	if !exists {
		return errors.New("Function dosen't exist")
	}
	pipe.functions[position] = nFuntionPtr
	pipe.sdk.SetFunctionsPipeline(pipe.functions...)
	return nil
}

/* MQTT Config */
var mqttAdresable models.Addressable
var mqttKeyCertPair transforms.KeyCertPair

/* Utils */

func createMqttSender(lg logger.LoggingClient) (mqttSender *transforms.MQTTSender) {

	fmt.Println(mqttAdresable)
	fmt.Println(mqttKeyCertPair)

	if mqttAdresable.Protocol == "tls" {
		mqttConfig := transforms.MqttConfig{SkipCertVerify: true}
		mqttSender = transforms.NewMQTTSender(lg, mqttAdresable, &mqttKeyCertPair, mqttConfig, true)
	} else {
		mqttConfig := transforms.MqttConfig{}
		mqttSender = transforms.NewMQTTSender(lg, mqttAdresable, nil, mqttConfig, true)
	}

	return
}

func parseMqttConfig(appConfig map[string]string) {

	if appName, ok := appConfig["ApplicationName"]; ok {
		mqttAdresable.Publisher = appName
	}

	if address, ok := appConfig["MqttAddress"]; ok {
		mqttAdresable.Address = address
	}
	if port, ok := appConfig["MqttPort"]; ok {

		port, err := strconv.Atoi(port)
		if err == nil {
			mqttAdresable.Port = port
		}

	}

	if protocol, ok := appConfig["MqttProtocol"]; ok {
		mqttAdresable.Protocol = protocol
	}
	if topic, ok := appConfig["MqttTopic"]; ok {
		mqttAdresable.Topic = topic
	}
	if user, ok := appConfig["MqttUser"]; ok {
		mqttAdresable.User = user
	}
	if pass, ok := appConfig["MqttPassword"]; ok {
		mqttAdresable.Password = pass
	}

	if key, ok := appConfig["MqttKey"]; ok {
		mqttKeyCertPair.KeyFile = key
	}
	if cert, ok := appConfig["MqttCert"]; ok {
		mqttKeyCertPair.CertFile = cert
	}

}

/*

InitDefaultPipeline inits and returns a pipeline with the default configuration
on res/configuration.toml

*/
func (pipe *ConfigurablePipeline) InitDefaultPipeline(sdk *appsdk.AppFunctionsSDK, appSettings map[string]string) {
	pipe.sdk = sdk
	parseMqttConfig(appSettings)

	/* Create MqttSender from config*/
	mqttSender := createMqttSender(pipe.sdk.LoggingClient)
	pipe.positions = make(map[string]int)
	pipe.addFunction("TransformToJSON", transforms.NewConversion().TransformToJSON)
	pipe.addFunction("postDataClay", postDataClay)
	pipe.addFunction("getReadingValue", getReadingValue)
	pipe.addFunction("MQTTSend", mqttSender.MQTTSend)
	pipe.addFunction("MarkAsPushed", transforms.NewCoreData().MarkAsPushed)
	pipe.sdk.SetFunctionsPipeline(pipe.functions...)
}

/* Public update functions */

/*
FilterByDevices Not Implemented
*/
func (pipe ConfigurablePipeline) FilterByDevices(devices []string) error {
	return errors.New("Not Implemented")
}

/*
UpdateMQTTConfig updates the creates a new MqttSender with the parameters and overwrites the pipeline
*/
func (pipe ConfigurablePipeline) UpdateMQTTConfig(mqttConfig models.Addressable) {
	if pipe.sdk == nil {
		fmt.Println("Pipeline is not inited")
		os.Exit(-1)
	}

	needToUpdate := false

	if mqttConfig.Address != "" {
		mqttAdresable.Address = mqttConfig.Address
		needToUpdate = true
	}
	if mqttConfig.Port != 0 {
		mqttAdresable.Port = mqttConfig.Port
		needToUpdate = true
	}
	if mqttConfig.Protocol != "" {
		mqttAdresable.Protocol = mqttConfig.Protocol
		needToUpdate = true
	}
	if mqttConfig.Topic != "" {
		mqttAdresable.Topic = mqttConfig.Topic
		needToUpdate = true
	}

	if needToUpdate {
		sender := createMqttSender(pipe.sdk.LoggingClient)
		pipe.updateFunction("MQTTSend", sender.MQTTSend)
	} else {
		fmt.Println("Keeping the same pipeline")
	}
}
