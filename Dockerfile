

#build stage
ARG BASE=golang:1.12-alpine
FROM ${BASE} AS builder

ARG ALPINE_PKG_BASE="make git gcc libc-dev libsodium-dev zeromq-dev"
ARG ALPINE_PKG_EXTRA=""

LABEL license='SPDX-License-Identifier: Apache-2.0' \
  copyright='Copyright (c) 2019: Intel'
RUN sed -e 's/dl-cdn[.]alpinelinux.org/nl.alpinelinux.org/g' -i~ /etc/apk/repositories
RUN apk add --no-cache ${ALPINE_PKG_BASE} ${ALPINE_PKG_EXTRA}
WORKDIR /app

COPY go.mod .
RUN go mod download

COPY . .
RUN go build .


#final stage
FROM alpine:latest
LABEL license='SPDX-License-Identifier: Apache-2.0' \
  copyright='Copyright (c) 2019: Intel'
LABEL Name=konnect-app-functions Version=${VERSION}

RUN apk --no-cache add ca-certificates zeromq
COPY --from=builder /app/res/docker/ /res/
COPY --from=builder /app/konn_appf_service /konn_appf_service
EXPOSE 48095

ENTRYPOINT ["/konn_appf_service","--confdir=/res"]
