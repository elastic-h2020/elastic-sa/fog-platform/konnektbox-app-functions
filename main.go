//
// Copyright (c) 2019 Intel Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/edgexfoundry/app-functions-sdk-go/appsdk"
	"github.com/edgexfoundry/go-mod-core-contracts/models"
	"konn_appf_service/cnfpipe"
)

const (
	serviceKey = "konnektAppFunctionsService"
)

var edgexSdk *appsdk.AppFunctionsSDK
var pipeline cnfpipe.ConfigurablePipeline

func updateMqttConfig(writer http.ResponseWriter, req *http.Request) {

	var mqtt models.Addressable
	mqtt.Name = "MqttClientConfig"

	err := json.NewDecoder(req.Body).Decode(&mqtt)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	pipeline.UpdateMQTTConfig(mqtt)
	fmt.Fprintf(writer, "MqttConfig Updated!\n")
	fmt.Fprintf(writer, "Current config:\n")
	fmt.Fprintf(writer, "%#v\n", mqtt)

	fmt.Print("CONFIGURED!")

}

func main() {
	edgexSdk = &appsdk.AppFunctionsSDK{ServiceKey: serviceKey}
	if err := edgexSdk.Initialize(); err != nil {
		fmt.Printf("SDK initialization failed: %v\n", err)
		os.Exit(-1)
	}

	/* Add API REST Routes */
	edgexSdk.AddRoute("/mqtt", updateMqttConfig, "POST")

	appSettings := edgexSdk.ApplicationSettings()
	if appSettings != nil {
		appName, ok := appSettings["ApplicationName"]
		if ok {
			edgexSdk.LoggingClient.Info(fmt.Sprintf("%s now running...", appName))
		} else {
			edgexSdk.LoggingClient.Error("ApplicationName application setting not found")
			os.Exit(-1)
		}
	} else {
		edgexSdk.LoggingClient.Error("No application settings found")
		os.Exit(-1)
	}

	/* Init pipeline with configuration */
	pipeline.InitDefaultPipeline(edgexSdk, appSettings)

	err := edgexSdk.MakeItRun()
	if err != nil {
		edgexSdk.LoggingClient.Error("MakeItRun returned error: ", err.Error())
		os.Exit(-1)
	}

	// Do any required cleanup here

	os.Exit(0)
}
